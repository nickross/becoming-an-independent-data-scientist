#!/usr/bin/env python
#
# Read in historical All Blacks scrores from the Wikipedia page
#
import os
import csv
import requests
from bs4 import BeautifulSoup

FILENAME = 'ab_scores.csv'
ENDPOINT = 'https://en.wikipedia.org/wiki/List_of_New_Zealand_rugby_union_Test_matches'


def main():
    count = 0
    cur_dir = os.path.dirname(os.path.realpath(__file__))
    with open(os.path.join(cur_dir, FILENAME), 'w') as fh:
        writer = csv.DictWriter(fh, fieldnames=['date', 'opponent', 'winner'])
        writer.writeheader()
        soup = BeautifulSoup(requests.get(ENDPOINT).text, 'html.parser')
        for table in soup.findAll('table', {'class': 'wikitable'})[1:]:
            body = table.find('tbody')
            for row in body.findAll('tr'):
                cells = row.findAll('td')
                if len(cells) > 0 and len(cells[6].findAll('a')) > 0:
                    writer.writerow({
                        'date': cells[0].text.rstrip(),
                        'opponent': cells[1].text.rstrip(),
                        'winner': cells[6].find('a')['title'],
                    })
                    count += 1
    print('Fetched {} scores'.format(count))


if __name__ == '__main__':
    main()
